import { Module } from '@nestjs/common';
import { FirebaseService } from './firebase.service';
import firebase from 'firebase-admin';
import serviceAccount from './partymy-c089b-firebase-adminsdk-dnjak-b26357bf89.json';

export const FIREBASE = 'FIREBASE';

const firebaseConfig = {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  credential: firebase.credential.cert(serviceAccount),
  apiKey: 'AIzaSyCWuQU5dYVgJQnQtywpcbQXi23WQ3AFAgI',
  authDomain: 'partymy-c089b.firebaseapp.com',
  projectId: 'partymy-c089b',
  storageBucket: 'partymy-c089b.appspot.com',
};

@Module({
  providers: [
    {
      provide: FIREBASE,
      useFactory: () => {
        firebase.initializeApp(firebaseConfig);
        return firebase;
      },
    },
    FirebaseService,
  ],
  exports: [FirebaseService],
})
export class FirebaseModule {}
