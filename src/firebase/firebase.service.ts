import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import firebase from 'firebase-admin';
import { IUploadAvatar } from '../profile/interfaces/upload-avatar.interface';

@Injectable()
export class FirebaseService {
  storage: firebase.storage.Storage;

  constructor(@Inject('FIREBASE') firebase) {
    this.storage = firebase.storage();
  }

  async uploadFile(data): Promise<IUploadAvatar> {
    const bucket = this.storage.bucket();
    if (data && data.path) {
      const response = await bucket.upload(data.path, {
        destination: `avatars/${data.filename}`,
        contentType: data.mimeType,
        public: true,
      });
      return { avatarLink: response[0].metadata.mediaLink };
    }
  }
}
