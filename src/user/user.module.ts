import { Module } from '@nestjs/common';
import { UserSchema } from './shemas/user.shemas';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TokenModule } from '../token/token.module';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [
    TokenModule,
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
  ],
  providers: [UserService, MailModule],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
