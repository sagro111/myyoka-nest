export interface IContacts {
    readonly tel: string
    readonly twitter: string
    readonly telegram: string
    readonly facebook: string
    readonly vk: string
}
