export interface IAddress {
  readonly country: string;
  readonly city: string;
}
