import { Document } from 'mongoose';
import { IAddress } from './adress.interface';
import { statusEnum } from '../enums/status.enum';
import { IContacts } from './contacts.interface';

export interface IUser extends Document {
  readonly email: string;
  readonly password?: string;
  readonly avatar?: string;
  readonly lastName: string;
  readonly firstName: string;
  readonly role: Array<string>;
  readonly birthday: Date;
  readonly city: string;
  readonly country: string;

  readonly contacts: IContacts;
  readonly address: IAddress;
  readonly gender: string;

  status: statusEnum;
}
