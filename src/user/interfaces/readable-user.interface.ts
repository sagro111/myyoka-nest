
export interface IReadableUser {
  readonly email: string;
  readonly lastName: string;
  readonly firstName: string;
  readonly role: Array<string>
  accessToken?: string;
  readonly status: string;
}
