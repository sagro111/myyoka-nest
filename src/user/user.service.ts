import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from './interfaces/user.interface';
import { CreateUserDto } from './dto/create-user.dto';
import * as bcrypt from 'bcrypt';
import _ from 'lodash';
import { roleEnum } from './enums/role.enum';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private readonly userModel: Model<IUser>) {}

  async create(
    createUserDto: CreateUserDto,
    roles: Array<string> = [roleEnum.guest],
  ): Promise<IUser> {
    const saltSize = 10;
    const salt = await bcrypt.genSalt(saltSize);
    const hash = await bcrypt.hash(createUserDto.password, salt);

    const createdUser = new this.userModel(
      _.assignIn(createUserDto, { password: hash, roles }),
    );

    return await createdUser.save();
  }

  async update(updatedUser: IUser, _id: string) {
    return this.userModel.replaceOne({ _id }, updatedUser, {});
  }

  async findByLogin(login: string): Promise<IUser> {
    return await this.userModel.findById(login).exec();
  }

  async find(id: string): Promise<IUser> {
    return await this.userModel.findById(id).exec();
  }

  async findByEmail(email: string): Promise<IUser> {
    return this.userModel.findOne({ email: email }).exec();
  }
}
