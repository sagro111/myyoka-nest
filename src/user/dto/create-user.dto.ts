import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { IsConfirmation } from '../decorators/is-confirmations.decorator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  @IsString()
  readonly email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(18)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak',
  })
  readonly password: string;

  @IsConfirmation('password', { message: 'Пароли должны совпадать' })
  @IsNotEmpty()
  @IsString()
  readonly passwordConfirmation: string;

  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  readonly lastName: string;

  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  readonly firstName: string;

  @IsNotEmpty()
  @ApiProperty()
  @IsBoolean()
  readonly terms: boolean;
}
