import * as mongoose from 'mongoose';
import { roleEnum } from '../enums/role.enum';
import { statusEnum } from '../enums/status.enum';
import { genderEnum } from '../enums/gender.enum';

export const UserSchema = new mongoose.Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  lastName: { type: String, required: true },
  firstName: { type: String, required: true },
  terms: { type: Boolean, required: true },
  status: {
    type: String,
    default: statusEnum.pending,
    enum: Object.values(statusEnum),
  },
  role: {
    type: [String],
    default: roleEnum.guest,
    enum: Object.values(roleEnum),
  },
  avatar: { type: Object },
  contacts: { type: Array, default: null },
  country: { type: String, default: null },
  city: { type: String, default: null },
  gender: { type: String, enum: Object.values(genderEnum) },
  birthday: { type: String },
  tel: { type: String },
});
UserSchema.index({ email: 1 }, { unique: true });
