import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  MethodNotAllowedException,
  UnauthorizedException,
} from '@nestjs/common';
import bcrypt from 'bcrypt';
import { UserService } from '../user/user.service';
import { IUser } from '../user/interfaces/user.interface';
import { TokenService } from '../token/token.service';
import { JwtService, JwtSignOptions } from '@nestjs/jwt';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { CreateUserTokenDto } from '../token/dto/create-user-token.dto';
import moment from 'moment';
import { ConfigService } from '@nestjs/config';
import { statusEnum } from '../user/enums/status.enum';
import { IReadableUser } from '../user/interfaces/readable-user.interface';
import { SignInDto } from './dto/sign-in.dto';
import { ITokenPayload } from '../token/interfaces/token-payload.interface';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { MailService } from '../mail/mail.service';

@Injectable()
export class AuthService {
  private readonly clientUrl: string;
  private readonly adminEmail: string;

  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
    private readonly tokenService: TokenService,
    private readonly mailService: MailService,
    private readonly configService: ConfigService,
  ) {
    this.clientUrl = this.configService.get<string>('FRONT_URL');
    this.adminEmail = this.configService.get<string>('ADMIN_EMAIL');
  }

  async signUp(createUserDto: CreateUserDto): Promise<boolean> {
    const isUserContains = await this.userService.findByEmail(
      createUserDto.email,
    );
    if (!isUserContains) {
      const user = await this.userService.create(createUserDto);
      await this.sendConfirmation(user);
      return true;
    }

    throw new HttpException(
      {
        message: [
          {
            email: ['Пользователь с таким Email уже существует'],
          },
        ],
        status: HttpStatus.UNAUTHORIZED,
      },
      HttpStatus.UNPROCESSABLE_ENTITY,
    );
  }

  async signIn(signInDto: SignInDto): Promise<string> {
    const user: IUser = await this.userService.findByEmail(signInDto.email);

    if (!user) {
      throw new UnauthorizedException('User with this email does not exist');
    }

    const isComparePass = await bcrypt.compare(
      signInDto.password,
      user.password,
    );

    if (user && isComparePass) {
      if (user.status === statusEnum.pending) {
        throw new UnauthorizedException('Confirm your email please');
      }
      if (user.status === statusEnum.blocked) {
        throw new UnauthorizedException('You are banned');
      }

      const readableUser: IReadableUser = user.toObject();
      readableUser.accessToken = await this.signUser(user);

      return readableUser.accessToken;
    }
    throw new UnauthorizedException('Invalid cred');
  }

  async signUser(user: IUser, checkStatus = true) {
    if (checkStatus && user.status !== statusEnum.active) {
      throw new MethodNotAllowedException();
    }

    const tokenPayload: ITokenPayload = {
      _id: user._id,
      status: user.status,
      role: user.role,
      avatar: user.avatar,
      username: `${user.firstName} ${user.lastName}`,
    };

    const token = await this.generateToken(tokenPayload);
    const expiresAt = moment().add(1, 'day').toISOString();

    await this.saveToken({
      token,
      expiresAt,
      uuid: user._id,
    });

    return token;
  }

  async confirm(token: string) {
    const data = await this.verifyToken(token);
    const user = await this.userService.find(data._id);

    await this.tokenService.delete(data._id, token);

    if (user && user.status === statusEnum.pending) {
      user.status = statusEnum.active;
      return user.save();
    }

    throw new BadRequestException('Confirmation error');
  }

  async sendConfirmation(user: IUser) {
    const token = await this.signUser(user, false);
    const confirmLink = `${this.clientUrl}/_impersonate/?token=${token}`;

    await this.mailService.sendMail({
      from: this.adminEmail,
      to: user.email,
      subject: 'Verify your account',
      html: `
                <h3>Hi ${user.firstName}</h3>
                <p>Please verify your account, visit this <a href="${confirmLink}">Link</a></p>
              `,
    });
  }

  async forgotPassword(forgotPasswordDto: ForgotPasswordDto): Promise<void> {
    const user: IUser = await this.userService.findByEmail(
      forgotPasswordDto.email,
    );
    const token = await this.signUser(user);
    const forgotLink = `${this.clientUrl}/auth/forgotPassword?token=${token}`;

    await this.mailService.sendMail({
      from: this.adminEmail,
      to: user.email,
      subject: 'Forgot password?',
      html: `
                Link <a href="${forgotLink}"> set new password</a>
            `,
    });
  }

  async saveToken(createUserTokenDto: CreateUserTokenDto) {
    await this.tokenService.create(createUserTokenDto);
  }

  async validateUser(login: string, password: string): Promise<Partial<IUser>> {
    const user = await this.userService.findByLogin(login);

    if (user && user.password === password) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  private async generateToken(
    data: ITokenPayload,
    options?: JwtSignOptions,
  ): Promise<string> {
    return this.jwtService.sign(data, options);
  }

  private async verifyToken(token): Promise<any> {
    try {
      const data = this.jwtService.verify(token);
      const tokenExist = this.tokenService.exist(data._id, token);
      if (tokenExist) {
        return data;
      }
      throw new UnauthorizedException();
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
}
