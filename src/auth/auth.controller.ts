import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { ApiTags } from '@nestjs/swagger';
import { ConfirmAccountDto } from './dto/confirm-account.dto';
import { SignInDto } from './dto/sign-in.dto';
import { ForgotPasswordDto } from './dto/forgot-password.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('sign_up')
  async signUp(@Body() createUserDto: CreateUserDto): Promise<boolean> {
    return await this.authService.signUp(createUserDto);
  }

  @Post('confirm')
  async confirm(@Body() confirmAccountDto: ConfirmAccountDto) {
    return await this.authService.confirm(confirmAccountDto.token);
  }

  @Post('sign_in')
  async signIn(@Body() signInDto: SignInDto): Promise<string> {
    return await this.authService.signIn(signInDto);
  }

  @Post('forgot_password')
  async forgotPassword(
    @Body() forgotPasswordDto: ForgotPasswordDto,
  ): Promise<void> {
    return this.authService.forgotPassword(forgotPasswordDto);
  }
}
