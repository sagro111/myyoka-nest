import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { TokenService } from '../token/token.service';
import { IUser } from '../user/interfaces/user.interface';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { stringify } from 'flatted';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly configService: ConfigService,
        private readonly tokenService: TokenService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get('JWT_SECRET_KEY'),
            passReqToCallback: true,
        });
    }

    async validate(req, user: Partial<IUser>): Promise<Partial<string>> {
        const token = req.headers.authorization.slice(7);

        const tokenExist = await this.tokenService.exist(user._id, token);

        if (tokenExist) {
            return stringify(user);
        }

        throw new UnauthorizedException();
    }
}
