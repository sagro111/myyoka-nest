import { Module } from '@nestjs/common';
import { TokenService } from './token.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TokenScheme } from './schema/token.schemas';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Token', schema: TokenScheme }]),
  ],
  providers: [TokenService],
  exports: [TokenService],
})
export class TokenModule {}
