export interface ITokenPayload {
  _id: string;
  status: string;
  avatar: string;
  role: Array<string>;
  username: string;
}
