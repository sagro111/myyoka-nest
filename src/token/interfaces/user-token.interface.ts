import { Document } from 'mongoose';

export interface IUserToken extends Document {
  token: string;
  uuid: string;
  expiresAt: string;
}
