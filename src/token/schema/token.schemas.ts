import * as mongoose from 'mongoose';

export const TokenScheme = new mongoose.Schema({
  token: { type: String, required: true },
  uuid: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
  expiresAt: { type: Date, required: true },
});

TokenScheme.index({ token: 1, uuid: 1 }, { unique: true });
