import { IsDateString, IsString } from 'class-validator';
import * as mongoose from 'mongoose';

export class CreateUserTokenDto {
  @IsString()
  token: string;
  @IsString()
  uuid: mongoose.Schema.Types.ObjectId;
  @IsDateString()
  expiresAt: string;
}
