import { Injectable, ValidationError, ValidationPipe } from '@nestjs/common';
import { iterate } from 'iterare';

@Injectable()
export class MainValidationPipe extends ValidationPipe {
  constructor(props) {
    super(props);
  }

  protected flattenValidationErrors(validationErrors: ValidationError[]): any {
    return iterate(validationErrors)
      .map((error) => this.mapChildrenToValidationErrors(error))
      .flatten()
      .filter((item) => !!item.constraints)
      .map((item) => {
        return {
          [item.property]: Object.values(item.constraints).map((key) =>
            key.replace(`${item.property} `, ''),
          ),
        };
      })
      .flatten()
      .toArray();
  }
}
