import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { IMailData } from './interfaces/mail.interfase';

@Injectable()
export class MailService {
  constructor(private readonly mailService: MailerService) {}

  public async sendMail(data: IMailData): Promise<void> {
    await this.mailService.sendMail(data).catch((e) => console.log(e));
  }
}
