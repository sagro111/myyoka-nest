import { Module } from '@nestjs/common';
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';
import { UserModule } from '../user/user.module';
import { TokenModule } from '../token/token.module';
import { FirebaseModule } from '../firebase/firebase.module';

@Module({
  imports: [UserModule, TokenModule, FirebaseModule],
  controllers: [ProfileController],
  providers: [ProfileService],
})
export class ProfileModule {}
