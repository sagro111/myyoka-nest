export enum profileSensitiveEnum {
  'VERSION' = '__v',
  'PASSWORD' = 'password',
  'ROLE' = 'role',
  'ID' = '_id',
}
