import { IsNotEmpty, IsObject } from 'class-validator';
import * as FormData from 'form-data';

export class UploadAvatarDto {
  @IsNotEmpty()
  @IsObject()
  readonly avatar: FormData;
}
