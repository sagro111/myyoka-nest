import { IsEmail, IsNotEmpty, IsObject, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { genderEnum } from '../../user/enums/gender.enum';
import * as FormData from 'form-data';

export class UpdateProfileDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  @IsString()
  readonly email: string;

  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  readonly lastName: string;

  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  readonly firstName: string;

  @ApiProperty()
  readonly birthday: Date;

  @ApiProperty()
  readonly avatar: string;

  @ApiProperty()
  readonly country: string;

  @ApiProperty()
  readonly city: string;

  @ApiProperty()
  readonly tel: string;

  @ApiProperty()
  readonly gender: genderEnum;

  @ApiProperty()
  readonly socials: Array<any>;
}
