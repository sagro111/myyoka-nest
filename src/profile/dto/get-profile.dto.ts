import { IsNotEmpty, IsString } from 'class-validator';

export class GetProfileDto {
    @IsNotEmpty()
    @IsString()
    _id: string
}
