export interface IUploadAvatar {
  readonly avatarLink: string;
}
