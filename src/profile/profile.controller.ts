import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth-guard.service';
import { ProfileService } from './profile.service';
import { IUser } from '../user/interfaces/user.interface';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { IUploadAvatar } from './interfaces/upload-avatar.interface';
import { UploadAvatarDto } from './dto/upload-avatar.dto';
import { diskStorage } from 'multer';
import e from 'express';
import path from 'path';
import { v4 as uuid5 } from 'uuid';

@Controller('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @UseGuards(JwtAuthGuard)
  @Post('upload_avatar')
  @UseInterceptors(
    FileInterceptor('avatar', {
      storage: diskStorage({
        destination: './upload/avatars/',
        filename(
          req: e.Request,
          file: Express.Multer.File,
          callback: (error: Error | null, filename: string) => void,
        ) {
          const filename: string =
            path.parse(file.originalname).name.replace(/\s/g, '' + '') +
            uuid5();
          const extension: string = path.parse(file.originalname).ext;

          callback(null, `${filename}${extension}`);
        },
      }),
    }),
  )
  async uploadAvatar(
    @UploadedFile() avatar: UploadAvatarDto,
  ): Promise<IUploadAvatar> {
    return await this.profileService.uploadAvatar(avatar);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/:id')
  async getProfile(@Param() params): Promise<Partial<IUser>> {
    return await this.profileService.getProfile(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/:id')
  async updateProfile(
    @Param() params,
    @Body() updateProfileDto: UpdateProfileDto,
  ): Promise<Partial<IUser>> {
    return await this.profileService.updateProfile(updateProfileDto, params.id);
  }
}
