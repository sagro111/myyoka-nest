import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { statusEnum } from '../user/enums/status.enum';
import { IUser } from '../user/interfaces/user.interface';
import _ from 'lodash';
import { profileSensitiveEnum } from './enum/profile-sensitive.enum';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { IUploadAvatar } from './interfaces/upload-avatar.interface';
import { UploadAvatarDto } from './dto/upload-avatar.dto';
import { FirebaseService } from '../firebase/firebase.service';

@Injectable()
export class ProfileService {
  constructor(
    private readonly userService: UserService,
    private readonly firebase: FirebaseService,
  ) {}

  async getProfile(id: string): Promise<Partial<IUser>> {
    const user = await this.userService.find(id);

    if (user && user.status === statusEnum.active && user.password) {
      return _.omit(user.toObject(), Object.values(profileSensitiveEnum));
    }
    throw new UnauthorizedException();
  }

  async updateProfile(
    data: UpdateProfileDto,
    id: string,
  ): Promise<Partial<IUser>> {
    const user = await this.userService.find(id);
    const updatedUser = ({ ...user.toObject(), ...data } as unknown) as IUser;
    await this.userService.update(updatedUser, id);

    return updatedUser;
  }

  async uploadAvatar(avatar: UploadAvatarDto): Promise<IUploadAvatar> {
    return this.firebase.uploadFile(avatar);
  }
}
